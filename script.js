function gradeConverter(perc){
	if (typeof(perc) != "number" || perc > 100 || perc < 0){
		console.log("Please enter a number between 0 and 100.")
	}
	else{
		if (perc >= 80 && perc <= 100){
			console.log("Your grade is A");
		}
		else if (perc >= 75 && perc <= 79){
			console.log("Your grade is B+");
		}
		else if (perc >= 65 && perc <= 74){
			console.log("Your grade is B");
		}
		else if (perc >= 55 && perc <= 64){
			console.log("Your grade is C+");
		}
		else if (perc >= 50 && perc <= 54){
			console.log("Your grade is C");
		}
		else if (perc >= 40 && perc <= 49){
			console.log("Your grade is D");
		}
		else{
			console.log("Your grade is F");
		}
	}
}
